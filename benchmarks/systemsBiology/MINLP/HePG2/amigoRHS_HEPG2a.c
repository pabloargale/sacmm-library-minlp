#include <amigoRHS.h>
#include <AMIGO_model.h>
#include <math.h>

#define STAT3 Ith(y,0)
#define TRAF6 Ith(y,1)
#define akt Ith(y,2)
#define MP2K2MP2K1 Ith(y,3)
#define p38 Ith(y,4)
#define MTOR Ith(y,5)
#define M3K7 Ith(y,6)
#define MK03MK01 Ith(y,7)
#define RASK Ith(y,8)
#define M3K1 Ith(y,9)
#define KS6A1 Ith(y,10)
#define MK08MK09 Ith(y,11)
#define MP2K4 Ith(y,12)
#define pi3k Ith(y,13)
#define IKBA Ith(y,14)
#define ikk Ith(y,15)
#define JUN Ith(y,16)
#define KS6A5KS6A4 Ith(y,17)
#define IRS1_s Ith(y,18)
#define KS6B1 Ith(y,19)
#define HSPB1 Ith(y,20)
#define GSK3AGSK3B Ith(y,21)
#define creb Ith(y,22)
#define P53 Ith(y,23)
#define H31TH33 Ith(y,24)
#define dSTAT3 Ith(ydot,0)
#define dTRAF6 Ith(ydot,1)
#define dakt Ith(ydot,2)
#define dMP2K2MP2K1 Ith(ydot,3)
#define dp38 Ith(ydot,4)
#define dMTOR Ith(ydot,5)
#define dM3K7 Ith(ydot,6)
#define dMK03MK01 Ith(ydot,7)
#define dRASK Ith(ydot,8)
#define dM3K1 Ith(ydot,9)
#define dKS6A1 Ith(ydot,10)
#define dMK08MK09 Ith(ydot,11)
#define dMP2K4 Ith(ydot,12)
#define dpi3k Ith(ydot,13)
#define dIKBA Ith(ydot,14)
#define dikk Ith(ydot,15)
#define dJUN Ith(ydot,16)
#define dKS6A5KS6A4 Ith(ydot,17)
#define dIRS1_s Ith(ydot,18)
#define dKS6B1 Ith(ydot,19)
#define dHSPB1 Ith(ydot,20)
#define dGSK3AGSK3B Ith(ydot,21)
#define dcreb Ith(ydot,22)
#define dP53 Ith(ydot,23)
#define dH31TH33 Ith(ydot,24)
#define IFNG (amigo_model->controls_v[0][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[0][(*amigo_model).index_t_stim])
#define TNFA (amigo_model->controls_v[1][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[1][(*amigo_model).index_t_stim])
#define IL1A (amigo_model->controls_v[2][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[2][(*amigo_model).index_t_stim])
#define IL6 (amigo_model->controls_v[3][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[3][(*amigo_model).index_t_stim])
#define IGF1 (amigo_model->controls_v[4][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[4][(*amigo_model).index_t_stim])
#define TGFA (amigo_model->controls_v[5][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[5][(*amigo_model).index_t_stim])
#define lps (amigo_model->controls_v[6][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[6][(*amigo_model).index_t_stim])
#define MP2K2MP2K1_Inhibitor (amigo_model->controls_v[7][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[7][(*amigo_model).index_t_stim])
#define p38_Inhibitor (amigo_model->controls_v[8][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[8][(*amigo_model).index_t_stim])
#define pi3k_Inhibitor (amigo_model->controls_v[9][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[9][(*amigo_model).index_t_stim])
#define ikk_Inhibitor (amigo_model->controls_v[10][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[10][(*amigo_model).index_t_stim])
#define MTOR_Inhibitor (amigo_model->controls_v[11][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[11][(*amigo_model).index_t_stim])
#define GSK3AGSK3B_Inhibitor (amigo_model->controls_v[12][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[12][(*amigo_model).index_t_stim])
#define MK08MK09_Inhibitor (amigo_model->controls_v[13][(*amigo_model).index_t_stim]+(t-(*amigo_model).tlast)*(*amigo_model).slope[13][(*amigo_model).index_t_stim])
#define n_IL6_fHN_STAT3 (amigo_model->pars[0])
#define k_IL6_fHN_STAT3 (amigo_model->pars[1])
#define tau_STAT3 (amigo_model->pars[2])
#define n_lps_fHN_TRAF6 (amigo_model->pars[3])
#define k_lps_fHN_TRAF6 (amigo_model->pars[4])
#define n_IL1A_fHN_TRAF6 (amigo_model->pars[5])
#define k_IL1A_fHN_TRAF6 (amigo_model->pars[6])
#define tau_TRAF6 (amigo_model->pars[7])
#define n_pi3k_fHN_akt (amigo_model->pars[8])
#define k_pi3k_fHN_akt (amigo_model->pars[9])
#define tau_akt (amigo_model->pars[10])
#define n_TRAF6_fHN_MP2K2MP2K1 (amigo_model->pars[11])
#define k_TRAF6_fHN_MP2K2MP2K1 (amigo_model->pars[12])
#define n_akt_fHN_MP2K2MP2K1 (amigo_model->pars[13])
#define k_akt_fHN_MP2K2MP2K1 (amigo_model->pars[14])
#define n_RASK_fHN_MP2K2MP2K1 (amigo_model->pars[15])
#define k_RASK_fHN_MP2K2MP2K1 (amigo_model->pars[16])
#define n_pi3k_fHN_MP2K2MP2K1 (amigo_model->pars[17])
#define k_pi3k_fHN_MP2K2MP2K1 (amigo_model->pars[18])
#define tau_MP2K2MP2K1 (amigo_model->pars[19])
#define n_M3K7_fHN_p38 (amigo_model->pars[20])
#define k_M3K7_fHN_p38 (amigo_model->pars[21])
#define n_MP2K4_fHN_p38 (amigo_model->pars[22])
#define k_MP2K4_fHN_p38 (amigo_model->pars[23])
#define tau_p38 (amigo_model->pars[24])
#define n_akt_fHN_MTOR (amigo_model->pars[25])
#define k_akt_fHN_MTOR (amigo_model->pars[26])
#define tau_MTOR (amigo_model->pars[27])
#define n_TRAF6_fHN_M3K7 (amigo_model->pars[28])
#define k_TRAF6_fHN_M3K7 (amigo_model->pars[29])
#define n_TNFA_fHN_M3K7 (amigo_model->pars[30])
#define k_TNFA_fHN_M3K7 (amigo_model->pars[31])
#define tau_M3K7 (amigo_model->pars[32])
#define n_MP2K2MP2K1_fHN_MK03MK01 (amigo_model->pars[33])
#define k_MP2K2MP2K1_fHN_MK03MK01 (amigo_model->pars[34])
#define tau_MK03MK01 (amigo_model->pars[35])
#define n_akt_fHN_RASK (amigo_model->pars[36])
#define k_akt_fHN_RASK (amigo_model->pars[37])
#define n_MK03MK01_fHN_RASK (amigo_model->pars[38])
#define k_MK03MK01_fHN_RASK (amigo_model->pars[39])
#define n_TGFA_fHN_RASK (amigo_model->pars[40])
#define k_TGFA_fHN_RASK (amigo_model->pars[41])
#define n_IGF1_fHN_RASK (amigo_model->pars[42])
#define k_IGF1_fHN_RASK (amigo_model->pars[43])
#define tau_RASK (amigo_model->pars[44])
#define n_TRAF6_fHN_M3K1 (amigo_model->pars[45])
#define k_TRAF6_fHN_M3K1 (amigo_model->pars[46])
#define n_RASK_fHN_M3K1 (amigo_model->pars[47])
#define k_RASK_fHN_M3K1 (amigo_model->pars[48])
#define n_pi3k_fHN_M3K1 (amigo_model->pars[49])
#define k_pi3k_fHN_M3K1 (amigo_model->pars[50])
#define tau_M3K1 (amigo_model->pars[51])
#define n_MP2K2MP2K1_fHN_KS6A1 (amigo_model->pars[52])
#define k_MP2K2MP2K1_fHN_KS6A1 (amigo_model->pars[53])
#define tau_KS6A1 (amigo_model->pars[54])
#define n_M3K7_fHN_MK08MK09 (amigo_model->pars[55])
#define k_M3K7_fHN_MK08MK09 (amigo_model->pars[56])
#define n_M3K1_fHN_MK08MK09 (amigo_model->pars[57])
#define k_M3K1_fHN_MK08MK09 (amigo_model->pars[58])
#define n_MP2K4_fHN_MK08MK09 (amigo_model->pars[59])
#define k_MP2K4_fHN_MK08MK09 (amigo_model->pars[60])
#define n_TNFA_fHN_MK08MK09 (amigo_model->pars[61])
#define k_TNFA_fHN_MK08MK09 (amigo_model->pars[62])
#define tau_MK08MK09 (amigo_model->pars[63])
#define n_M3K7_fHN_MP2K4 (amigo_model->pars[64])
#define k_M3K7_fHN_MP2K4 (amigo_model->pars[65])
#define n_M3K1_fHN_MP2K4 (amigo_model->pars[66])
#define k_M3K1_fHN_MP2K4 (amigo_model->pars[67])
#define n_TNFA_fHN_MP2K4 (amigo_model->pars[68])
#define k_TNFA_fHN_MP2K4 (amigo_model->pars[69])
#define tau_MP2K4 (amigo_model->pars[70])
#define n_RASK_fHN_pi3k (amigo_model->pars[71])
#define k_RASK_fHN_pi3k (amigo_model->pars[72])
#define n_TGFA_fHN_pi3k (amigo_model->pars[73])
#define k_TGFA_fHN_pi3k (amigo_model->pars[74])
#define n_TNFA_fHN_pi3k (amigo_model->pars[75])
#define k_TNFA_fHN_pi3k (amigo_model->pars[76])
#define n_IGF1_fHN_pi3k (amigo_model->pars[77])
#define k_IGF1_fHN_pi3k (amigo_model->pars[78])
#define n_IRS1_s_fHN_pi3k (amigo_model->pars[79])
#define k_IRS1_s_fHN_pi3k (amigo_model->pars[80])
#define tau_pi3k (amigo_model->pars[81])
#define n_ikk_fHN_IKBA (amigo_model->pars[82])
#define k_ikk_fHN_IKBA (amigo_model->pars[83])
#define tau_IKBA (amigo_model->pars[84])
#define n_akt_fHN_ikk (amigo_model->pars[85])
#define k_akt_fHN_ikk (amigo_model->pars[86])
#define n_M3K7_fHN_ikk (amigo_model->pars[87])
#define k_M3K7_fHN_ikk (amigo_model->pars[88])
#define n_M3K1_fHN_ikk (amigo_model->pars[89])
#define k_M3K1_fHN_ikk (amigo_model->pars[90])
#define tau_ikk (amigo_model->pars[91])
#define n_MK08MK09_fHN_JUN (amigo_model->pars[92])
#define k_MK08MK09_fHN_JUN (amigo_model->pars[93])
#define tau_JUN (amigo_model->pars[94])
#define n_p38_fHN_KS6A5KS6A4 (amigo_model->pars[95])
#define k_p38_fHN_KS6A5KS6A4 (amigo_model->pars[96])
#define n_MK03MK01_fHN_KS6A5KS6A4 (amigo_model->pars[97])
#define k_MK03MK01_fHN_KS6A5KS6A4 (amigo_model->pars[98])
#define tau_KS6A5KS6A4 (amigo_model->pars[99])
#define n_MTOR_fHN_IRS1_s (amigo_model->pars[100])
#define k_MTOR_fHN_IRS1_s (amigo_model->pars[101])
#define n_MK03MK01_fHN_IRS1_s (amigo_model->pars[102])
#define k_MK03MK01_fHN_IRS1_s (amigo_model->pars[103])
#define tau_IRS1_s (amigo_model->pars[104])
#define n_MTOR_fHN_KS6B1 (amigo_model->pars[105])
#define k_MTOR_fHN_KS6B1 (amigo_model->pars[106])
#define n_MK03MK01_fHN_KS6B1 (amigo_model->pars[107])
#define k_MK03MK01_fHN_KS6B1 (amigo_model->pars[108])
#define n_pi3k_fHN_KS6B1 (amigo_model->pars[109])
#define k_pi3k_fHN_KS6B1 (amigo_model->pars[110])
#define tau_KS6B1 (amigo_model->pars[111])
#define n_p38_fHN_HSPB1 (amigo_model->pars[112])
#define k_p38_fHN_HSPB1 (amigo_model->pars[113])
#define n_M3K7_fHN_HSPB1 (amigo_model->pars[114])
#define k_M3K7_fHN_HSPB1 (amigo_model->pars[115])
#define n_MK03MK01_fHN_HSPB1 (amigo_model->pars[116])
#define k_MK03MK01_fHN_HSPB1 (amigo_model->pars[117])
#define tau_HSPB1 (amigo_model->pars[118])
#define n_akt_fHN_GSK3AGSK3B (amigo_model->pars[119])
#define k_akt_fHN_GSK3AGSK3B (amigo_model->pars[120])
#define tau_GSK3AGSK3B (amigo_model->pars[121])
#define n_KS6A1_fHN_creb (amigo_model->pars[122])
#define k_KS6A1_fHN_creb (amigo_model->pars[123])
#define n_KS6A5KS6A4_fHN_creb (amigo_model->pars[124])
#define k_KS6A5KS6A4_fHN_creb (amigo_model->pars[125])
#define tau_creb (amigo_model->pars[126])
#define n_akt_fHN_P53 (amigo_model->pars[127])
#define k_akt_fHN_P53 (amigo_model->pars[128])
#define n_MK08MK09_fHN_P53 (amigo_model->pars[129])
#define k_MK08MK09_fHN_P53 (amigo_model->pars[130])
#define tau_P53 (amigo_model->pars[131])
#define n_KS6A5KS6A4_fHN_H31TH33 (amigo_model->pars[132])
#define k_KS6A5KS6A4_fHN_H31TH33 (amigo_model->pars[133])
#define tau_H31TH33 (amigo_model->pars[134])
#define w1 (amigo_model->pars[135])
#define w2 (amigo_model->pars[136])
#define w3 (amigo_model->pars[137])
#define w4 (amigo_model->pars[138])
#define w5 (amigo_model->pars[139])
#define w6 (amigo_model->pars[140])
#define w7 (amigo_model->pars[141])
#define w8 (amigo_model->pars[142])
#define w9 (amigo_model->pars[143])
#define w10 (amigo_model->pars[144])
#define w11 (amigo_model->pars[145])
#define w12 (amigo_model->pars[146])
#define w13 (amigo_model->pars[147])
#define w14 (amigo_model->pars[148])
#define w15 (amigo_model->pars[149])
#define w16 (amigo_model->pars[150])
#define w17 (amigo_model->pars[151])
#define w18 (amigo_model->pars[152])
#define w19 (amigo_model->pars[153])
#define w20 (amigo_model->pars[154])
#define w21 (amigo_model->pars[155])
#define w22 (amigo_model->pars[156])
#define w23 (amigo_model->pars[157])
#define w24 (amigo_model->pars[158])
#define w25 (amigo_model->pars[159])
#define w26 (amigo_model->pars[160])
#define w27 (amigo_model->pars[161])
#define w28 (amigo_model->pars[162])
#define w29 (amigo_model->pars[163])
#define w30 (amigo_model->pars[164])
#define w31 (amigo_model->pars[165])
#define w32 (amigo_model->pars[166])
#define w33 (amigo_model->pars[167])
#define w34 (amigo_model->pars[168])
#define w35 (amigo_model->pars[169])
#define w36 (amigo_model->pars[170])
#define w37 (amigo_model->pars[171])
#define w38 (amigo_model->pars[172])
#define w39 (amigo_model->pars[173])
#define w40 (amigo_model->pars[174])
#define w41 (amigo_model->pars[175])
#define w42 (amigo_model->pars[176])
#define w43 (amigo_model->pars[177])
#define w44 (amigo_model->pars[178])
#define w45 (amigo_model->pars[179])
#define w46 (amigo_model->pars[180])
#define w47 (amigo_model->pars[181])
#define w48 (amigo_model->pars[182])
#define w49 (amigo_model->pars[183])
#define w50 (amigo_model->pars[184])
#define w51 (amigo_model->pars[185])
#define w52 (amigo_model->pars[186])
#define w53 (amigo_model->pars[187])
#define w54 (amigo_model->pars[188])
#define w55 (amigo_model->pars[189])
#define w56 (amigo_model->pars[190])
#define w57 (amigo_model->pars[191])
#define w58 (amigo_model->pars[192])
#define w59 (amigo_model->pars[193])
#define w60 (amigo_model->pars[194])
#define w61 (amigo_model->pars[195])
#define w62 (amigo_model->pars[196])
#define w63 (amigo_model->pars[197])
#define w64 (amigo_model->pars[198])
#define w65 (amigo_model->pars[199])
#define w66 (amigo_model->pars[200])
#define w67 (amigo_model->pars[201])
#define w68 (amigo_model->pars[202])
#define w69 (amigo_model->pars[203])
#define w70 (amigo_model->pars[204])
#define w71 (amigo_model->pars[205])
#define w72 (amigo_model->pars[206])
#define w73 (amigo_model->pars[207])
#define w74 (amigo_model->pars[208])
#define w75 (amigo_model->pars[209])
#define w76 (amigo_model->pars[210])
#define w77 (amigo_model->pars[211])
#define w78 (amigo_model->pars[212])
#define w79 (amigo_model->pars[213])
#define w80 (amigo_model->pars[214])
#define w81 (amigo_model->pars[215])
#define w82 (amigo_model->pars[216])
#define w83 (amigo_model->pars[217])
#define w84 (amigo_model->pars[218])
#define w85 (amigo_model->pars[219])
#define w86 (amigo_model->pars[220])
#define w87 (amigo_model->pars[221])
#define w88 (amigo_model->pars[222])
#define w89 (amigo_model->pars[223])
#define w90 (amigo_model->pars[224])
#define w91 (amigo_model->pars[225])
#define w92 (amigo_model->pars[226])
#define w93 (amigo_model->pars[227])
#define w94 (amigo_model->pars[228])
#define w95 (amigo_model->pars[229])
#define w96 (amigo_model->pars[230])
#define w97 (amigo_model->pars[231])
#define w98 (amigo_model->pars[232])
#define w99 (amigo_model->pars[233])
#define w100 (amigo_model->pars[234])
#define w101 (amigo_model->pars[235])
#define w102 (amigo_model->pars[236])
#define w103 (amigo_model->pars[237])
#define w104 (amigo_model->pars[238])
#define w105 (amigo_model->pars[239])
#define w106 (amigo_model->pars[240])
#define w107 (amigo_model->pars[241])
#define w108 (amigo_model->pars[242])
#define w109 (amigo_model->pars[243])
double OR1(double m1){
	 double f=
1-(
(1-m1));
;
return(f);
}
double OR2(double m1,double m2){
	 double f=
1-(
(1-m1)*
(1-m2));
;
return(f);
}
double OR3(double m1,double m2,double m3){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3));
;
return(f);
}
double OR4(double m1,double m2,double m3,double m4){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4));
;
return(f);
}
double OR5(double m1,double m2,double m3,double m4,double m5){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5));
;
return(f);
}
double OR6(double m1,double m2,double m3,double m4,double m5,double m6){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6));
;
return(f);
}
double OR7(double m1,double m2,double m3,double m4,double m5,double m6,double m7){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7));
;
return(f);
}
double OR8(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8));
;
return(f);
}
double OR9(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9));
;
return(f);
}
double OR10(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10));
;
return(f);
}
double OR11(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10,double m11){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10)*
(1-m11));
;
return(f);
}
double OR12(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10,double m11,double m12){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10)*
(1-m11)*
(1-m12));
;
return(f);
}
double OR13(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10,double m11,double m12,double m13){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10)*
(1-m11)*
(1-m12)*
(1-m13));
;
return(f);
}
double OR14(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10,double m11,double m12,double m13,double m14){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10)*
(1-m11)*
(1-m12)*
(1-m13)*
(1-m14));
;
return(f);
}
double OR15(double m1,double m2,double m3,double m4,double m5,double m6,double m7,double m8,double m9,double m10,double m11,double m12,double m13,double m14,double m15){
	 double f=
1-(
(1-m1)*
(1-m2)*
(1-m3)*
(1-m4)*
(1-m5)*
(1-m6)*
(1-m7)*
(1-m8)*
(1-m9)*
(1-m10)*
(1-m11)*
(1-m12)*
(1-m13)*
(1-m14)*
(1-m15));
;
return(f);
}
/* Right hand side of the system (f(t,x,p))*/
int amigoRHS_HEPG2a(realtype t, N_Vector y, N_Vector ydot, void *data){
AMIGO_model* amigo_model=(AMIGO_model*)data;

double IL6_fHN_STAT3;
double lps_fHN_TRAF6;
double IL1A_fHN_TRAF6;
double pi3k_fHN_akt;
double TRAF6_fHN_MP2K2MP2K1;
double RASK_fHN_MP2K2MP2K1;
double akt_fHN_MP2K2MP2K1;
double pi3k_fHN_MP2K2MP2K1;
double MP2K4_fHN_p38;
double M3K7_fHN_p38;
double akt_fHN_MTOR;
double TRAF6_fHN_M3K7;
double TNFA_fHN_M3K7;
double MP2K2MP2K1_fHN_MK03MK01;
double akt_fHN_RASK;
double TGFA_fHN_RASK;
double MK03MK01_fHN_RASK;
double IGF1_fHN_RASK;
double RASK_fHN_M3K1;
double pi3k_fHN_M3K1;
double TRAF6_fHN_M3K1;
double MP2K2MP2K1_fHN_KS6A1;
double MP2K4_fHN_MK08MK09;
double M3K1_fHN_MK08MK09;
double M3K7_fHN_MK08MK09;
double TNFA_fHN_MK08MK09;
double M3K1_fHN_MP2K4;
double M3K7_fHN_MP2K4;
double TNFA_fHN_MP2K4;
double RASK_fHN_pi3k;
double TGFA_fHN_pi3k;
double IRS1_s_fHN_pi3k;
double IGF1_fHN_pi3k;
double TNFA_fHN_pi3k;
double ikk_fHN_IKBA;
double M3K1_fHN_ikk;
double akt_fHN_ikk;
double M3K7_fHN_ikk;
double MK08MK09_fHN_JUN;
double MK03MK01_fHN_KS6A5KS6A4;
double p38_fHN_KS6A5KS6A4;
double MTOR_fHN_IRS1_s;
double MK03MK01_fHN_IRS1_s;
double MTOR_fHN_KS6B1;
double MK03MK01_fHN_KS6B1;
double pi3k_fHN_KS6B1;
double p38_fHN_HSPB1;
double MK03MK01_fHN_HSPB1;
double M3K7_fHN_HSPB1;
double akt_fHN_GSK3AGSK3B;
double KS6A5KS6A4_fHN_creb;
double KS6A1_fHN_creb;
double MK08MK09_fHN_P53;
double akt_fHN_P53;
double KS6A5KS6A4_fHN_H31TH33;
IL6_fHN_STAT3=(pow(IL6,n_IL6_fHN_STAT3)/(pow(IL6,n_IL6_fHN_STAT3)+pow(k_IL6_fHN_STAT3,n_IL6_fHN_STAT3)))/(1/(1+pow(k_IL6_fHN_STAT3,n_IL6_fHN_STAT3)));
lps_fHN_TRAF6=(pow(lps,n_lps_fHN_TRAF6)/(pow(lps,n_lps_fHN_TRAF6)+pow(k_lps_fHN_TRAF6,n_lps_fHN_TRAF6)))/(1/(1+pow(k_lps_fHN_TRAF6,n_lps_fHN_TRAF6)));
IL1A_fHN_TRAF6=(pow(IL1A,n_IL1A_fHN_TRAF6)/(pow(IL1A,n_IL1A_fHN_TRAF6)+pow(k_IL1A_fHN_TRAF6,n_IL1A_fHN_TRAF6)))/(1/(1+pow(k_IL1A_fHN_TRAF6,n_IL1A_fHN_TRAF6)));
pi3k_fHN_akt=(pow(pi3k,n_pi3k_fHN_akt)/(pow(pi3k,n_pi3k_fHN_akt)+pow(k_pi3k_fHN_akt,n_pi3k_fHN_akt)))/(1/(1+pow(k_pi3k_fHN_akt,n_pi3k_fHN_akt)));
TRAF6_fHN_MP2K2MP2K1=(pow(TRAF6,n_TRAF6_fHN_MP2K2MP2K1)/(pow(TRAF6,n_TRAF6_fHN_MP2K2MP2K1)+pow(k_TRAF6_fHN_MP2K2MP2K1,n_TRAF6_fHN_MP2K2MP2K1)))/(1/(1+pow(k_TRAF6_fHN_MP2K2MP2K1,n_TRAF6_fHN_MP2K2MP2K1)));
RASK_fHN_MP2K2MP2K1=(pow(RASK,n_RASK_fHN_MP2K2MP2K1)/(pow(RASK,n_RASK_fHN_MP2K2MP2K1)+pow(k_RASK_fHN_MP2K2MP2K1,n_RASK_fHN_MP2K2MP2K1)))/(1/(1+pow(k_RASK_fHN_MP2K2MP2K1,n_RASK_fHN_MP2K2MP2K1)));
akt_fHN_MP2K2MP2K1=(pow(akt,n_akt_fHN_MP2K2MP2K1)/(pow(akt,n_akt_fHN_MP2K2MP2K1)+pow(k_akt_fHN_MP2K2MP2K1,n_akt_fHN_MP2K2MP2K1)))/(1/(1+pow(k_akt_fHN_MP2K2MP2K1,n_akt_fHN_MP2K2MP2K1)));
pi3k_fHN_MP2K2MP2K1=(pow(pi3k,n_pi3k_fHN_MP2K2MP2K1)/(pow(pi3k,n_pi3k_fHN_MP2K2MP2K1)+pow(k_pi3k_fHN_MP2K2MP2K1,n_pi3k_fHN_MP2K2MP2K1)))/(1/(1+pow(k_pi3k_fHN_MP2K2MP2K1,n_pi3k_fHN_MP2K2MP2K1)));
MP2K4_fHN_p38=(pow(MP2K4,n_MP2K4_fHN_p38)/(pow(MP2K4,n_MP2K4_fHN_p38)+pow(k_MP2K4_fHN_p38,n_MP2K4_fHN_p38)))/(1/(1+pow(k_MP2K4_fHN_p38,n_MP2K4_fHN_p38)));
M3K7_fHN_p38=(pow(M3K7,n_M3K7_fHN_p38)/(pow(M3K7,n_M3K7_fHN_p38)+pow(k_M3K7_fHN_p38,n_M3K7_fHN_p38)))/(1/(1+pow(k_M3K7_fHN_p38,n_M3K7_fHN_p38)));
akt_fHN_MTOR=(pow(akt,n_akt_fHN_MTOR)/(pow(akt,n_akt_fHN_MTOR)+pow(k_akt_fHN_MTOR,n_akt_fHN_MTOR)))/(1/(1+pow(k_akt_fHN_MTOR,n_akt_fHN_MTOR)));
TRAF6_fHN_M3K7=(pow(TRAF6,n_TRAF6_fHN_M3K7)/(pow(TRAF6,n_TRAF6_fHN_M3K7)+pow(k_TRAF6_fHN_M3K7,n_TRAF6_fHN_M3K7)))/(1/(1+pow(k_TRAF6_fHN_M3K7,n_TRAF6_fHN_M3K7)));
TNFA_fHN_M3K7=(pow(TNFA,n_TNFA_fHN_M3K7)/(pow(TNFA,n_TNFA_fHN_M3K7)+pow(k_TNFA_fHN_M3K7,n_TNFA_fHN_M3K7)))/(1/(1+pow(k_TNFA_fHN_M3K7,n_TNFA_fHN_M3K7)));
MP2K2MP2K1_fHN_MK03MK01=(pow(MP2K2MP2K1,n_MP2K2MP2K1_fHN_MK03MK01)/(pow(MP2K2MP2K1,n_MP2K2MP2K1_fHN_MK03MK01)+pow(k_MP2K2MP2K1_fHN_MK03MK01,n_MP2K2MP2K1_fHN_MK03MK01)))/(1/(1+pow(k_MP2K2MP2K1_fHN_MK03MK01,n_MP2K2MP2K1_fHN_MK03MK01)));
akt_fHN_RASK=(pow(akt,n_akt_fHN_RASK)/(pow(akt,n_akt_fHN_RASK)+pow(k_akt_fHN_RASK,n_akt_fHN_RASK)))/(1/(1+pow(k_akt_fHN_RASK,n_akt_fHN_RASK)));
TGFA_fHN_RASK=(pow(TGFA,n_TGFA_fHN_RASK)/(pow(TGFA,n_TGFA_fHN_RASK)+pow(k_TGFA_fHN_RASK,n_TGFA_fHN_RASK)))/(1/(1+pow(k_TGFA_fHN_RASK,n_TGFA_fHN_RASK)));
MK03MK01_fHN_RASK=(pow(MK03MK01,n_MK03MK01_fHN_RASK)/(pow(MK03MK01,n_MK03MK01_fHN_RASK)+pow(k_MK03MK01_fHN_RASK,n_MK03MK01_fHN_RASK)))/(1/(1+pow(k_MK03MK01_fHN_RASK,n_MK03MK01_fHN_RASK)));
IGF1_fHN_RASK=(pow(IGF1,n_IGF1_fHN_RASK)/(pow(IGF1,n_IGF1_fHN_RASK)+pow(k_IGF1_fHN_RASK,n_IGF1_fHN_RASK)))/(1/(1+pow(k_IGF1_fHN_RASK,n_IGF1_fHN_RASK)));
RASK_fHN_M3K1=(pow(RASK,n_RASK_fHN_M3K1)/(pow(RASK,n_RASK_fHN_M3K1)+pow(k_RASK_fHN_M3K1,n_RASK_fHN_M3K1)))/(1/(1+pow(k_RASK_fHN_M3K1,n_RASK_fHN_M3K1)));
pi3k_fHN_M3K1=(pow(pi3k,n_pi3k_fHN_M3K1)/(pow(pi3k,n_pi3k_fHN_M3K1)+pow(k_pi3k_fHN_M3K1,n_pi3k_fHN_M3K1)))/(1/(1+pow(k_pi3k_fHN_M3K1,n_pi3k_fHN_M3K1)));
TRAF6_fHN_M3K1=(pow(TRAF6,n_TRAF6_fHN_M3K1)/(pow(TRAF6,n_TRAF6_fHN_M3K1)+pow(k_TRAF6_fHN_M3K1,n_TRAF6_fHN_M3K1)))/(1/(1+pow(k_TRAF6_fHN_M3K1,n_TRAF6_fHN_M3K1)));
MP2K2MP2K1_fHN_KS6A1=(pow(MP2K2MP2K1,n_MP2K2MP2K1_fHN_KS6A1)/(pow(MP2K2MP2K1,n_MP2K2MP2K1_fHN_KS6A1)+pow(k_MP2K2MP2K1_fHN_KS6A1,n_MP2K2MP2K1_fHN_KS6A1)))/(1/(1+pow(k_MP2K2MP2K1_fHN_KS6A1,n_MP2K2MP2K1_fHN_KS6A1)));
MP2K4_fHN_MK08MK09=(pow(MP2K4,n_MP2K4_fHN_MK08MK09)/(pow(MP2K4,n_MP2K4_fHN_MK08MK09)+pow(k_MP2K4_fHN_MK08MK09,n_MP2K4_fHN_MK08MK09)))/(1/(1+pow(k_MP2K4_fHN_MK08MK09,n_MP2K4_fHN_MK08MK09)));
M3K1_fHN_MK08MK09=(pow(M3K1,n_M3K1_fHN_MK08MK09)/(pow(M3K1,n_M3K1_fHN_MK08MK09)+pow(k_M3K1_fHN_MK08MK09,n_M3K1_fHN_MK08MK09)))/(1/(1+pow(k_M3K1_fHN_MK08MK09,n_M3K1_fHN_MK08MK09)));
M3K7_fHN_MK08MK09=(pow(M3K7,n_M3K7_fHN_MK08MK09)/(pow(M3K7,n_M3K7_fHN_MK08MK09)+pow(k_M3K7_fHN_MK08MK09,n_M3K7_fHN_MK08MK09)))/(1/(1+pow(k_M3K7_fHN_MK08MK09,n_M3K7_fHN_MK08MK09)));
TNFA_fHN_MK08MK09=(pow(TNFA,n_TNFA_fHN_MK08MK09)/(pow(TNFA,n_TNFA_fHN_MK08MK09)+pow(k_TNFA_fHN_MK08MK09,n_TNFA_fHN_MK08MK09)))/(1/(1+pow(k_TNFA_fHN_MK08MK09,n_TNFA_fHN_MK08MK09)));
M3K1_fHN_MP2K4=(pow(M3K1,n_M3K1_fHN_MP2K4)/(pow(M3K1,n_M3K1_fHN_MP2K4)+pow(k_M3K1_fHN_MP2K4,n_M3K1_fHN_MP2K4)))/(1/(1+pow(k_M3K1_fHN_MP2K4,n_M3K1_fHN_MP2K4)));
M3K7_fHN_MP2K4=(pow(M3K7,n_M3K7_fHN_MP2K4)/(pow(M3K7,n_M3K7_fHN_MP2K4)+pow(k_M3K7_fHN_MP2K4,n_M3K7_fHN_MP2K4)))/(1/(1+pow(k_M3K7_fHN_MP2K4,n_M3K7_fHN_MP2K4)));
TNFA_fHN_MP2K4=(pow(TNFA,n_TNFA_fHN_MP2K4)/(pow(TNFA,n_TNFA_fHN_MP2K4)+pow(k_TNFA_fHN_MP2K4,n_TNFA_fHN_MP2K4)))/(1/(1+pow(k_TNFA_fHN_MP2K4,n_TNFA_fHN_MP2K4)));
RASK_fHN_pi3k=(pow(RASK,n_RASK_fHN_pi3k)/(pow(RASK,n_RASK_fHN_pi3k)+pow(k_RASK_fHN_pi3k,n_RASK_fHN_pi3k)))/(1/(1+pow(k_RASK_fHN_pi3k,n_RASK_fHN_pi3k)));
TGFA_fHN_pi3k=(pow(TGFA,n_TGFA_fHN_pi3k)/(pow(TGFA,n_TGFA_fHN_pi3k)+pow(k_TGFA_fHN_pi3k,n_TGFA_fHN_pi3k)))/(1/(1+pow(k_TGFA_fHN_pi3k,n_TGFA_fHN_pi3k)));
IRS1_s_fHN_pi3k=(pow(IRS1_s,n_IRS1_s_fHN_pi3k)/(pow(IRS1_s,n_IRS1_s_fHN_pi3k)+pow(k_IRS1_s_fHN_pi3k,n_IRS1_s_fHN_pi3k)))/(1/(1+pow(k_IRS1_s_fHN_pi3k,n_IRS1_s_fHN_pi3k)));
IGF1_fHN_pi3k=(pow(IGF1,n_IGF1_fHN_pi3k)/(pow(IGF1,n_IGF1_fHN_pi3k)+pow(k_IGF1_fHN_pi3k,n_IGF1_fHN_pi3k)))/(1/(1+pow(k_IGF1_fHN_pi3k,n_IGF1_fHN_pi3k)));
TNFA_fHN_pi3k=(pow(TNFA,n_TNFA_fHN_pi3k)/(pow(TNFA,n_TNFA_fHN_pi3k)+pow(k_TNFA_fHN_pi3k,n_TNFA_fHN_pi3k)))/(1/(1+pow(k_TNFA_fHN_pi3k,n_TNFA_fHN_pi3k)));
ikk_fHN_IKBA=(pow(ikk,n_ikk_fHN_IKBA)/(pow(ikk,n_ikk_fHN_IKBA)+pow(k_ikk_fHN_IKBA,n_ikk_fHN_IKBA)))/(1/(1+pow(k_ikk_fHN_IKBA,n_ikk_fHN_IKBA)));
M3K1_fHN_ikk=(pow(M3K1,n_M3K1_fHN_ikk)/(pow(M3K1,n_M3K1_fHN_ikk)+pow(k_M3K1_fHN_ikk,n_M3K1_fHN_ikk)))/(1/(1+pow(k_M3K1_fHN_ikk,n_M3K1_fHN_ikk)));
akt_fHN_ikk=(pow(akt,n_akt_fHN_ikk)/(pow(akt,n_akt_fHN_ikk)+pow(k_akt_fHN_ikk,n_akt_fHN_ikk)))/(1/(1+pow(k_akt_fHN_ikk,n_akt_fHN_ikk)));
M3K7_fHN_ikk=(pow(M3K7,n_M3K7_fHN_ikk)/(pow(M3K7,n_M3K7_fHN_ikk)+pow(k_M3K7_fHN_ikk,n_M3K7_fHN_ikk)))/(1/(1+pow(k_M3K7_fHN_ikk,n_M3K7_fHN_ikk)));
MK08MK09_fHN_JUN=(pow(MK08MK09,n_MK08MK09_fHN_JUN)/(pow(MK08MK09,n_MK08MK09_fHN_JUN)+pow(k_MK08MK09_fHN_JUN,n_MK08MK09_fHN_JUN)))/(1/(1+pow(k_MK08MK09_fHN_JUN,n_MK08MK09_fHN_JUN)));
MK03MK01_fHN_KS6A5KS6A4=(pow(MK03MK01,n_MK03MK01_fHN_KS6A5KS6A4)/(pow(MK03MK01,n_MK03MK01_fHN_KS6A5KS6A4)+pow(k_MK03MK01_fHN_KS6A5KS6A4,n_MK03MK01_fHN_KS6A5KS6A4)))/(1/(1+pow(k_MK03MK01_fHN_KS6A5KS6A4,n_MK03MK01_fHN_KS6A5KS6A4)));
p38_fHN_KS6A5KS6A4=(pow(p38,n_p38_fHN_KS6A5KS6A4)/(pow(p38,n_p38_fHN_KS6A5KS6A4)+pow(k_p38_fHN_KS6A5KS6A4,n_p38_fHN_KS6A5KS6A4)))/(1/(1+pow(k_p38_fHN_KS6A5KS6A4,n_p38_fHN_KS6A5KS6A4)));
MTOR_fHN_IRS1_s=(pow(MTOR,n_MTOR_fHN_IRS1_s)/(pow(MTOR,n_MTOR_fHN_IRS1_s)+pow(k_MTOR_fHN_IRS1_s,n_MTOR_fHN_IRS1_s)))/(1/(1+pow(k_MTOR_fHN_IRS1_s,n_MTOR_fHN_IRS1_s)));
MK03MK01_fHN_IRS1_s=(pow(MK03MK01,n_MK03MK01_fHN_IRS1_s)/(pow(MK03MK01,n_MK03MK01_fHN_IRS1_s)+pow(k_MK03MK01_fHN_IRS1_s,n_MK03MK01_fHN_IRS1_s)))/(1/(1+pow(k_MK03MK01_fHN_IRS1_s,n_MK03MK01_fHN_IRS1_s)));
MTOR_fHN_KS6B1=(pow(MTOR,n_MTOR_fHN_KS6B1)/(pow(MTOR,n_MTOR_fHN_KS6B1)+pow(k_MTOR_fHN_KS6B1,n_MTOR_fHN_KS6B1)))/(1/(1+pow(k_MTOR_fHN_KS6B1,n_MTOR_fHN_KS6B1)));
MK03MK01_fHN_KS6B1=(pow(MK03MK01,n_MK03MK01_fHN_KS6B1)/(pow(MK03MK01,n_MK03MK01_fHN_KS6B1)+pow(k_MK03MK01_fHN_KS6B1,n_MK03MK01_fHN_KS6B1)))/(1/(1+pow(k_MK03MK01_fHN_KS6B1,n_MK03MK01_fHN_KS6B1)));
pi3k_fHN_KS6B1=(pow(pi3k,n_pi3k_fHN_KS6B1)/(pow(pi3k,n_pi3k_fHN_KS6B1)+pow(k_pi3k_fHN_KS6B1,n_pi3k_fHN_KS6B1)))/(1/(1+pow(k_pi3k_fHN_KS6B1,n_pi3k_fHN_KS6B1)));
p38_fHN_HSPB1=(pow(p38,n_p38_fHN_HSPB1)/(pow(p38,n_p38_fHN_HSPB1)+pow(k_p38_fHN_HSPB1,n_p38_fHN_HSPB1)))/(1/(1+pow(k_p38_fHN_HSPB1,n_p38_fHN_HSPB1)));
MK03MK01_fHN_HSPB1=(pow(MK03MK01,n_MK03MK01_fHN_HSPB1)/(pow(MK03MK01,n_MK03MK01_fHN_HSPB1)+pow(k_MK03MK01_fHN_HSPB1,n_MK03MK01_fHN_HSPB1)))/(1/(1+pow(k_MK03MK01_fHN_HSPB1,n_MK03MK01_fHN_HSPB1)));
M3K7_fHN_HSPB1=(pow(M3K7,n_M3K7_fHN_HSPB1)/(pow(M3K7,n_M3K7_fHN_HSPB1)+pow(k_M3K7_fHN_HSPB1,n_M3K7_fHN_HSPB1)))/(1/(1+pow(k_M3K7_fHN_HSPB1,n_M3K7_fHN_HSPB1)));
akt_fHN_GSK3AGSK3B=(pow(akt,n_akt_fHN_GSK3AGSK3B)/(pow(akt,n_akt_fHN_GSK3AGSK3B)+pow(k_akt_fHN_GSK3AGSK3B,n_akt_fHN_GSK3AGSK3B)))/(1/(1+pow(k_akt_fHN_GSK3AGSK3B,n_akt_fHN_GSK3AGSK3B)));
KS6A5KS6A4_fHN_creb=(pow(KS6A5KS6A4,n_KS6A5KS6A4_fHN_creb)/(pow(KS6A5KS6A4,n_KS6A5KS6A4_fHN_creb)+pow(k_KS6A5KS6A4_fHN_creb,n_KS6A5KS6A4_fHN_creb)))/(1/(1+pow(k_KS6A5KS6A4_fHN_creb,n_KS6A5KS6A4_fHN_creb)));
KS6A1_fHN_creb=(pow(KS6A1,n_KS6A1_fHN_creb)/(pow(KS6A1,n_KS6A1_fHN_creb)+pow(k_KS6A1_fHN_creb,n_KS6A1_fHN_creb)))/(1/(1+pow(k_KS6A1_fHN_creb,n_KS6A1_fHN_creb)));
MK08MK09_fHN_P53=(pow(MK08MK09,n_MK08MK09_fHN_P53)/(pow(MK08MK09,n_MK08MK09_fHN_P53)+pow(k_MK08MK09_fHN_P53,n_MK08MK09_fHN_P53)))/(1/(1+pow(k_MK08MK09_fHN_P53,n_MK08MK09_fHN_P53)));
akt_fHN_P53=(pow(akt,n_akt_fHN_P53)/(pow(akt,n_akt_fHN_P53)+pow(k_akt_fHN_P53,n_akt_fHN_P53)))/(1/(1+pow(k_akt_fHN_P53,n_akt_fHN_P53)));
KS6A5KS6A4_fHN_H31TH33=(pow(KS6A5KS6A4,n_KS6A5KS6A4_fHN_H31TH33)/(pow(KS6A5KS6A4,n_KS6A5KS6A4_fHN_H31TH33)+pow(k_KS6A5KS6A4_fHN_H31TH33,n_KS6A5KS6A4_fHN_H31TH33)))/(1/(1+pow(k_KS6A5KS6A4_fHN_H31TH33,n_KS6A5KS6A4_fHN_H31TH33)));



dSTAT3=
(
0+
IL6_fHN_STAT3*OR1(w1)
-STAT3)*tau_STAT3;

dTRAF6=
(
0+
(1-lps_fHN_TRAF6)*IL1A_fHN_TRAF6*OR1(w3)+
lps_fHN_TRAF6*(1-IL1A_fHN_TRAF6)*OR1(w2)+
lps_fHN_TRAF6*IL1A_fHN_TRAF6*OR3(w2,w3,w4)
-TRAF6)*tau_TRAF6;

dakt=
(
0+
pi3k_fHN_akt*OR1(w5)
-akt)*tau_akt;

dMP2K2MP2K1=
(
(1-TRAF6_fHN_MP2K2MP2K1)*(1-RASK_fHN_MP2K2MP2K1)*(1-akt_fHN_MP2K2MP2K1)*(1-pi3k_fHN_MP2K2MP2K1)*OR1(w8)+
(1-TRAF6_fHN_MP2K2MP2K1)*(1-RASK_fHN_MP2K2MP2K1)*(1-akt_fHN_MP2K2MP2K1)*pi3k_fHN_MP2K2MP2K1*OR3(w8,w9,w15)+
0+
(1-TRAF6_fHN_MP2K2MP2K1)*(1-RASK_fHN_MP2K2MP2K1)*akt_fHN_MP2K2MP2K1*pi3k_fHN_MP2K2MP2K1*OR1(w9)+
(1-TRAF6_fHN_MP2K2MP2K1)*RASK_fHN_MP2K2MP2K1*(1-akt_fHN_MP2K2MP2K1)*(1-pi3k_fHN_MP2K2MP2K1)*OR3(w7,w8,w13)+
(1-TRAF6_fHN_MP2K2MP2K1)*RASK_fHN_MP2K2MP2K1*(1-akt_fHN_MP2K2MP2K1)*pi3k_fHN_MP2K2MP2K1*OR6(w7,w8,w9,w13,w14,w15)+
(1-TRAF6_fHN_MP2K2MP2K1)*RASK_fHN_MP2K2MP2K1*akt_fHN_MP2K2MP2K1*(1-pi3k_fHN_MP2K2MP2K1)*OR1(w7)+
(1-TRAF6_fHN_MP2K2MP2K1)*RASK_fHN_MP2K2MP2K1*akt_fHN_MP2K2MP2K1*pi3k_fHN_MP2K2MP2K1*OR3(w7,w9,w14)+
TRAF6_fHN_MP2K2MP2K1*(1-RASK_fHN_MP2K2MP2K1)*(1-akt_fHN_MP2K2MP2K1)*(1-pi3k_fHN_MP2K2MP2K1)*OR3(w6,w8,w11)+
TRAF6_fHN_MP2K2MP2K1*(1-RASK_fHN_MP2K2MP2K1)*(1-akt_fHN_MP2K2MP2K1)*pi3k_fHN_MP2K2MP2K1*OR6(w6,w8,w9,w11,w12,w15)+
TRAF6_fHN_MP2K2MP2K1*(1-RASK_fHN_MP2K2MP2K1)*akt_fHN_MP2K2MP2K1*(1-pi3k_fHN_MP2K2MP2K1)*OR1(w6)+
TRAF6_fHN_MP2K2MP2K1*(1-RASK_fHN_MP2K2MP2K1)*akt_fHN_MP2K2MP2K1*pi3k_fHN_MP2K2MP2K1*OR3(w6,w9,w12)+
TRAF6_fHN_MP2K2MP2K1*RASK_fHN_MP2K2MP2K1*(1-akt_fHN_MP2K2MP2K1)*(1-pi3k_fHN_MP2K2MP2K1)*OR6(w6,w7,w8,w10,w11,w13)+
TRAF6_fHN_MP2K2MP2K1*RASK_fHN_MP2K2MP2K1*(1-akt_fHN_MP2K2MP2K1)*pi3k_fHN_MP2K2MP2K1*OR10(w6,w7,w8,w9,w10,w11,w12,w13,w14,w15)+
TRAF6_fHN_MP2K2MP2K1*RASK_fHN_MP2K2MP2K1*akt_fHN_MP2K2MP2K1*(1-pi3k_fHN_MP2K2MP2K1)*OR3(w6,w7,w10)+
TRAF6_fHN_MP2K2MP2K1*RASK_fHN_MP2K2MP2K1*akt_fHN_MP2K2MP2K1*pi3k_fHN_MP2K2MP2K1*OR6(w6,w7,w9,w10,w12,w14)
-MP2K2MP2K1)*tau_MP2K2MP2K1*(1-MP2K2MP2K1_Inhibitor);

dp38=
(
0+
(1-MP2K4_fHN_p38)*M3K7_fHN_p38*OR1(w17)+
MP2K4_fHN_p38*(1-M3K7_fHN_p38)*OR1(w16)+
MP2K4_fHN_p38*M3K7_fHN_p38*OR3(w16,w17,w18)
-p38)*tau_p38*(1-p38_Inhibitor);

dMTOR=
(
0+
akt_fHN_MTOR*OR1(w19)
-MTOR)*tau_MTOR*(1-MTOR_Inhibitor);

dM3K7=
(
0+
(1-TRAF6_fHN_M3K7)*TNFA_fHN_M3K7*OR1(w21)+
TRAF6_fHN_M3K7*(1-TNFA_fHN_M3K7)*OR1(w20)+
TRAF6_fHN_M3K7*TNFA_fHN_M3K7*OR3(w20,w21,w22)
-M3K7)*tau_M3K7;

dMK03MK01=
(
0+
MP2K2MP2K1_fHN_MK03MK01*OR1(w23)
-MK03MK01)*tau_MK03MK01;

dRASK=
(
(1-akt_fHN_RASK)*(1-TGFA_fHN_RASK)*(1-MK03MK01_fHN_RASK)*(1-IGF1_fHN_RASK)*OR6(w24,w25,w26,w29,w30,w33)+
(1-akt_fHN_RASK)*(1-TGFA_fHN_RASK)*(1-MK03MK01_fHN_RASK)*IGF1_fHN_RASK*OR10(w24,w25,w26,w28,w29,w30,w32,w33,w34,w36)+
(1-akt_fHN_RASK)*(1-TGFA_fHN_RASK)*MK03MK01_fHN_RASK*(1-IGF1_fHN_RASK)*OR3(w24,w25,w29)+
(1-akt_fHN_RASK)*(1-TGFA_fHN_RASK)*MK03MK01_fHN_RASK*IGF1_fHN_RASK*OR6(w24,w25,w28,w29,w32,w34)+
(1-akt_fHN_RASK)*TGFA_fHN_RASK*(1-MK03MK01_fHN_RASK)*(1-IGF1_fHN_RASK)*OR6(w24,w26,w27,w30,w31,w35)+
(1-akt_fHN_RASK)*TGFA_fHN_RASK*(1-MK03MK01_fHN_RASK)*IGF1_fHN_RASK*OR10(w24,w26,w27,w28,w30,w31,w32,w35,w36,w37)+
(1-akt_fHN_RASK)*TGFA_fHN_RASK*MK03MK01_fHN_RASK*(1-IGF1_fHN_RASK)*OR3(w24,w27,w31)+
(1-akt_fHN_RASK)*TGFA_fHN_RASK*MK03MK01_fHN_RASK*IGF1_fHN_RASK*OR6(w24,w27,w28,w31,w32,w37)+
akt_fHN_RASK*(1-TGFA_fHN_RASK)*(1-MK03MK01_fHN_RASK)*(1-IGF1_fHN_RASK)*OR3(w25,w26,w33)+
akt_fHN_RASK*(1-TGFA_fHN_RASK)*(1-MK03MK01_fHN_RASK)*IGF1_fHN_RASK*OR6(w25,w26,w28,w33,w34,w36)+
akt_fHN_RASK*(1-TGFA_fHN_RASK)*MK03MK01_fHN_RASK*(1-IGF1_fHN_RASK)*OR1(w25)+
akt_fHN_RASK*(1-TGFA_fHN_RASK)*MK03MK01_fHN_RASK*IGF1_fHN_RASK*OR3(w25,w28,w34)+
akt_fHN_RASK*TGFA_fHN_RASK*(1-MK03MK01_fHN_RASK)*(1-IGF1_fHN_RASK)*OR3(w26,w27,w35)+
akt_fHN_RASK*TGFA_fHN_RASK*(1-MK03MK01_fHN_RASK)*IGF1_fHN_RASK*OR6(w26,w27,w28,w35,w36,w37)+
akt_fHN_RASK*TGFA_fHN_RASK*MK03MK01_fHN_RASK*(1-IGF1_fHN_RASK)*OR1(w27)+
akt_fHN_RASK*TGFA_fHN_RASK*MK03MK01_fHN_RASK*IGF1_fHN_RASK*OR3(w27,w28,w37)
-RASK)*tau_RASK;

dM3K1=
(
0+
(1-RASK_fHN_M3K1)*(1-pi3k_fHN_M3K1)*TRAF6_fHN_M3K1*OR1(w40)+
(1-RASK_fHN_M3K1)*pi3k_fHN_M3K1*(1-TRAF6_fHN_M3K1)*OR1(w39)+
(1-RASK_fHN_M3K1)*pi3k_fHN_M3K1*TRAF6_fHN_M3K1*OR3(w39,w40,w43)+
RASK_fHN_M3K1*(1-pi3k_fHN_M3K1)*(1-TRAF6_fHN_M3K1)*OR1(w38)+
RASK_fHN_M3K1*(1-pi3k_fHN_M3K1)*TRAF6_fHN_M3K1*OR3(w38,w40,w42)+
RASK_fHN_M3K1*pi3k_fHN_M3K1*(1-TRAF6_fHN_M3K1)*OR3(w38,w39,w41)+
RASK_fHN_M3K1*pi3k_fHN_M3K1*TRAF6_fHN_M3K1*OR6(w38,w39,w40,w41,w42,w43)
-M3K1)*tau_M3K1;

dKS6A1=
(
0+
MP2K2MP2K1_fHN_KS6A1*OR1(w44)
-KS6A1)*tau_KS6A1;

dMK08MK09=
(
0+
(1-MP2K4_fHN_MK08MK09)*(1-M3K1_fHN_MK08MK09)*(1-M3K7_fHN_MK08MK09)*TNFA_fHN_MK08MK09*OR1(w48)+
(1-MP2K4_fHN_MK08MK09)*(1-M3K1_fHN_MK08MK09)*M3K7_fHN_MK08MK09*(1-TNFA_fHN_MK08MK09)*OR1(w47)+
(1-MP2K4_fHN_MK08MK09)*(1-M3K1_fHN_MK08MK09)*M3K7_fHN_MK08MK09*TNFA_fHN_MK08MK09*OR3(w47,w48,w54)+
(1-MP2K4_fHN_MK08MK09)*M3K1_fHN_MK08MK09*(1-M3K7_fHN_MK08MK09)*(1-TNFA_fHN_MK08MK09)*OR1(w46)+
(1-MP2K4_fHN_MK08MK09)*M3K1_fHN_MK08MK09*(1-M3K7_fHN_MK08MK09)*TNFA_fHN_MK08MK09*OR3(w46,w48,w53)+
(1-MP2K4_fHN_MK08MK09)*M3K1_fHN_MK08MK09*M3K7_fHN_MK08MK09*(1-TNFA_fHN_MK08MK09)*OR3(w46,w47,w52)+
(1-MP2K4_fHN_MK08MK09)*M3K1_fHN_MK08MK09*M3K7_fHN_MK08MK09*TNFA_fHN_MK08MK09*OR6(w46,w47,w48,w52,w53,w54)+
MP2K4_fHN_MK08MK09*(1-M3K1_fHN_MK08MK09)*(1-M3K7_fHN_MK08MK09)*(1-TNFA_fHN_MK08MK09)*OR1(w45)+
MP2K4_fHN_MK08MK09*(1-M3K1_fHN_MK08MK09)*(1-M3K7_fHN_MK08MK09)*TNFA_fHN_MK08MK09*OR3(w45,w48,w51)+
MP2K4_fHN_MK08MK09*(1-M3K1_fHN_MK08MK09)*M3K7_fHN_MK08MK09*(1-TNFA_fHN_MK08MK09)*OR3(w45,w47,w50)+
MP2K4_fHN_MK08MK09*(1-M3K1_fHN_MK08MK09)*M3K7_fHN_MK08MK09*TNFA_fHN_MK08MK09*OR6(w45,w47,w48,w50,w51,w54)+
MP2K4_fHN_MK08MK09*M3K1_fHN_MK08MK09*(1-M3K7_fHN_MK08MK09)*(1-TNFA_fHN_MK08MK09)*OR3(w45,w46,w49)+
MP2K4_fHN_MK08MK09*M3K1_fHN_MK08MK09*(1-M3K7_fHN_MK08MK09)*TNFA_fHN_MK08MK09*OR6(w45,w46,w48,w49,w51,w53)+
MP2K4_fHN_MK08MK09*M3K1_fHN_MK08MK09*M3K7_fHN_MK08MK09*(1-TNFA_fHN_MK08MK09)*OR6(w45,w46,w47,w49,w50,w52)+
MP2K4_fHN_MK08MK09*M3K1_fHN_MK08MK09*M3K7_fHN_MK08MK09*TNFA_fHN_MK08MK09*OR10(w45,w46,w47,w48,w49,w50,w51,w52,w53,w54)
-MK08MK09)*tau_MK08MK09*(1-MK08MK09_Inhibitor);

dMP2K4=
(
0+
(1-M3K1_fHN_MP2K4)*(1-M3K7_fHN_MP2K4)*TNFA_fHN_MP2K4*OR1(w57)+
(1-M3K1_fHN_MP2K4)*M3K7_fHN_MP2K4*(1-TNFA_fHN_MP2K4)*OR1(w56)+
(1-M3K1_fHN_MP2K4)*M3K7_fHN_MP2K4*TNFA_fHN_MP2K4*OR3(w56,w57,w60)+
M3K1_fHN_MP2K4*(1-M3K7_fHN_MP2K4)*(1-TNFA_fHN_MP2K4)*OR1(w55)+
M3K1_fHN_MP2K4*(1-M3K7_fHN_MP2K4)*TNFA_fHN_MP2K4*OR3(w55,w57,w59)+
M3K1_fHN_MP2K4*M3K7_fHN_MP2K4*(1-TNFA_fHN_MP2K4)*OR3(w55,w56,w58)+
M3K1_fHN_MP2K4*M3K7_fHN_MP2K4*TNFA_fHN_MP2K4*OR6(w55,w56,w57,w58,w59,w60)
-MP2K4)*tau_MP2K4;

dpi3k=
(
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR1(w63)+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR3(w63,w65,w74)+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR3(w63,w64,w73)+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR6(w63,w64,w65,w73,w74,w75)+
0+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR1(w65)+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR1(w64)+
(1-RASK_fHN_pi3k)*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR3(w64,w65,w75)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR3(w62,w63,w70)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR6(w62,w63,w65,w70,w72,w74)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR6(w62,w63,w64,w70,w71,w73)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR10(w62,w63,w64,w65,w70,w71,w72,w73,w74,w75)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR1(w62)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR3(w62,w65,w72)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR3(w62,w64,w71)+
(1-RASK_fHN_pi3k)*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR6(w62,w64,w65,w71,w72,w75)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR3(w61,w63,w67)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR6(w61,w63,w65,w67,w69,w74)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR6(w61,w63,w64,w67,w68,w73)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR10(w61,w63,w64,w65,w67,w68,w69,w73,w74,w75)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR1(w61)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR3(w61,w65,w69)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR3(w61,w64,w68)+
RASK_fHN_pi3k*(1-TGFA_fHN_pi3k)*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR6(w61,w64,w65,w68,w69,w75)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR6(w61,w62,w63,w66,w67,w70)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR10(w61,w62,w63,w65,w66,w67,w69,w70,w72,w74)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR10(w61,w62,w63,w64,w66,w67,w68,w70,w71,w73)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*(1-IRS1_s_fHN_pi3k)*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR15(w61,w62,w63,w64,w65,w66,w67,w68,w69,w70,w71,w72,w73,w74,w75)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*(1-TNFA_fHN_pi3k)*OR3(w61,w62,w66)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*(1-IGF1_fHN_pi3k)*TNFA_fHN_pi3k*OR6(w61,w62,w65,w66,w69,w72)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*(1-TNFA_fHN_pi3k)*OR6(w61,w62,w64,w66,w68,w71)+
RASK_fHN_pi3k*TGFA_fHN_pi3k*IRS1_s_fHN_pi3k*IGF1_fHN_pi3k*TNFA_fHN_pi3k*OR10(w61,w62,w64,w65,w66,w68,w69,w71,w72,w75)
-pi3k)*tau_pi3k*(1-pi3k_Inhibitor);

dIKBA=
(
0+
ikk_fHN_IKBA*OR1(w76)
-IKBA)*tau_IKBA;

dikk=
(
0+
(1-M3K1_fHN_ikk)*(1-akt_fHN_ikk)*M3K7_fHN_ikk*OR1(w79)+
(1-M3K1_fHN_ikk)*akt_fHN_ikk*(1-M3K7_fHN_ikk)*OR1(w78)+
(1-M3K1_fHN_ikk)*akt_fHN_ikk*M3K7_fHN_ikk*OR3(w78,w79,w82)+
M3K1_fHN_ikk*(1-akt_fHN_ikk)*(1-M3K7_fHN_ikk)*OR1(w77)+
M3K1_fHN_ikk*(1-akt_fHN_ikk)*M3K7_fHN_ikk*OR3(w77,w79,w81)+
M3K1_fHN_ikk*akt_fHN_ikk*(1-M3K7_fHN_ikk)*OR3(w77,w78,w80)+
M3K1_fHN_ikk*akt_fHN_ikk*M3K7_fHN_ikk*OR6(w77,w78,w79,w80,w81,w82)
-ikk)*tau_ikk*(1-ikk_Inhibitor);

dJUN=
(
0+
MK08MK09_fHN_JUN*OR1(w83)
-JUN)*tau_JUN;

dKS6A5KS6A4=
(
0+
(1-MK03MK01_fHN_KS6A5KS6A4)*p38_fHN_KS6A5KS6A4*OR1(w85)+
MK03MK01_fHN_KS6A5KS6A4*(1-p38_fHN_KS6A5KS6A4)*OR1(w84)+
MK03MK01_fHN_KS6A5KS6A4*p38_fHN_KS6A5KS6A4*OR3(w84,w85,w86)
-KS6A5KS6A4)*tau_KS6A5KS6A4;

dIRS1_s=
(
0+
(1-MTOR_fHN_IRS1_s)*MK03MK01_fHN_IRS1_s*OR1(w88)+
MTOR_fHN_IRS1_s*(1-MK03MK01_fHN_IRS1_s)*OR1(w87)+
MTOR_fHN_IRS1_s*MK03MK01_fHN_IRS1_s*OR3(w87,w88,w89)
-IRS1_s)*tau_IRS1_s;

dKS6B1=
(
0+
(1-MTOR_fHN_KS6B1)*(1-MK03MK01_fHN_KS6B1)*pi3k_fHN_KS6B1*OR1(w92)+
(1-MTOR_fHN_KS6B1)*MK03MK01_fHN_KS6B1*(1-pi3k_fHN_KS6B1)*OR1(w91)+
(1-MTOR_fHN_KS6B1)*MK03MK01_fHN_KS6B1*pi3k_fHN_KS6B1*OR3(w91,w92,w95)+
MTOR_fHN_KS6B1*(1-MK03MK01_fHN_KS6B1)*(1-pi3k_fHN_KS6B1)*OR1(w90)+
MTOR_fHN_KS6B1*(1-MK03MK01_fHN_KS6B1)*pi3k_fHN_KS6B1*OR3(w90,w92,w94)+
MTOR_fHN_KS6B1*MK03MK01_fHN_KS6B1*(1-pi3k_fHN_KS6B1)*OR3(w90,w91,w93)+
MTOR_fHN_KS6B1*MK03MK01_fHN_KS6B1*pi3k_fHN_KS6B1*OR6(w90,w91,w92,w93,w94,w95)
-KS6B1)*tau_KS6B1;

dHSPB1=
(
0+
(1-p38_fHN_HSPB1)*(1-MK03MK01_fHN_HSPB1)*M3K7_fHN_HSPB1*OR1(w98)+
(1-p38_fHN_HSPB1)*MK03MK01_fHN_HSPB1*(1-M3K7_fHN_HSPB1)*OR1(w97)+
(1-p38_fHN_HSPB1)*MK03MK01_fHN_HSPB1*M3K7_fHN_HSPB1*OR3(w97,w98,w101)+
p38_fHN_HSPB1*(1-MK03MK01_fHN_HSPB1)*(1-M3K7_fHN_HSPB1)*OR1(w96)+
p38_fHN_HSPB1*(1-MK03MK01_fHN_HSPB1)*M3K7_fHN_HSPB1*OR3(w96,w98,w100)+
p38_fHN_HSPB1*MK03MK01_fHN_HSPB1*(1-M3K7_fHN_HSPB1)*OR3(w96,w97,w99)+
p38_fHN_HSPB1*MK03MK01_fHN_HSPB1*M3K7_fHN_HSPB1*OR6(w96,w97,w98,w99,w100,w101)
-HSPB1)*tau_HSPB1;

dGSK3AGSK3B=
(
0+
akt_fHN_GSK3AGSK3B*OR1(w102)
-GSK3AGSK3B)*tau_GSK3AGSK3B*(1-GSK3AGSK3B_Inhibitor);

dcreb=
(
0+
(1-KS6A5KS6A4_fHN_creb)*KS6A1_fHN_creb*OR1(w104)+
KS6A5KS6A4_fHN_creb*(1-KS6A1_fHN_creb)*OR1(w103)+
KS6A5KS6A4_fHN_creb*KS6A1_fHN_creb*OR3(w103,w104,w105)
-creb)*tau_creb;

dP53=
(
(1-MK08MK09_fHN_P53)*(1-akt_fHN_P53)*OR1(w107)+
0+
MK08MK09_fHN_P53*(1-akt_fHN_P53)*OR3(w106,w107,w108)+
MK08MK09_fHN_P53*akt_fHN_P53*OR1(w106)
-P53)*tau_P53;

dH31TH33=
(
0+
KS6A5KS6A4_fHN_H31TH33*OR1(w109)
-H31TH33)*tau_H31TH33;

return(0);
}

        
void amigoRHS_get_OBS_HEPG2a(void* data){

}

void amigoRHS_get_sens_OBS_HEPG2a(void* data){

}

void amigo_Y_at_tcon_HEPG2a(void* data, realtype t, N_Vector y){
    AMIGO_model* amigo_model=(AMIGO_model*)data;
    
}
