#include <Rinternals.h>
#include "AMIGO_model.h"
#include "AMIGO_problem.h"

AMIGO_problem* openRFileAMIGO(const char *);

void get_reduced_model(AMIGO_problem *, int );

//void RAMIGOmodelAlloc(SEXP , SEXP , SEXP ,int, AMIGO_model** );

int getListElement (SEXP , char *);
