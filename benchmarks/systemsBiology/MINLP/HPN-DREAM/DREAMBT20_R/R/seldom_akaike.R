
seldom_akaike<-function(x,model,exps,ivpsol,k,n_data){
  model$x[model$index_opt]=x;
  res=simulate_logic_based_ode(model,exps,ivpsol);
  if(is.infinite(res))res=1e10;
  return(2*k+n_data*log(res));
}