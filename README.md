# saCMM self-adaptive Cooperative Multimethod library  #

## self-adaptive Cooperative Multimethod (saCMM)##

This is version 0.2 of a novel parallel global optimization code, self-adaptive 
cooperative multimethod (saCMM). This code is distributed as a library with several parallel solvers based on the scatter search and differential evolution metaheuristics, incorporating several key new mechanisms: 

1. Asynchronous cooperation between parallel processes.
2. Coarse and fine-grained parallelism.
3. Self-tuning strategies.
4. Concurrent multimethods runs.

The saCMM code has been implemented using Fortran 90 and C. Parallelization has been implemented using MPI and openMP. It has been tested in Linux clusters running CentOS 6.7.
  
Previous versions of the saCMM library allowed the solution of non-linear programming (NLP) problems, but version 0.2 includes some extensions and modifications, hence it is also able to solve large MINLP problems. It provides efficient local solvers for NLP, but also an efficient mixed-integer local solver (MISQP) for MINLP problems. The current distribution of saCMM includes a set of optimization examples that can be used as benchmarks, taken from the BBOB and BioPreDyn testbeds, and three challenging problems for specific testing of the MINLP implementation.

For the reproduction of the results presented in the main paper, we recommend using example scripts located in the folder reproducibility_scripts.

## REFERENCES ##

### Main references: ###

P. González, P. Argüeso-Alejandro, D. R. Penas, X. C. Pardo, J. Sáez-Rodríguez, J.R. Banga, R. Doallo  Hybrid parallel multimethod hyperheuristic for mixed-integer dynamic optimization problems in computational systems biology (*Submitted for publication*).

P. Gonzalez, D.R. Penas, X.C. Pardo, J.R. Banga and R. Doallo (2018) Multimethod optimization in the Cloud: a case-study in Systems Biology modelling. Concurrency and Computation - Practice and Experience 30 (12)

### Related previous papers: ###

Penas, D.R., P. Gonzalez, J.A. Egea, R. Doallo and J.R. Banga (2017) Parameter estimation in large-scale systems biology models: a parallel and self-adaptive cooperative strategy. BMC Bioinformatics 18:52.

Penas DR, P Gonzalez, JA Egea, JR Banga, R Doallo (2015) Parallel Metaheuristics in Computational Biology: An Asynchronous Cooperative Enhanced Scatter Search Method. Procedia Computer Science, 51:630-639.

### Benchmark problems distributed with this library: ###

Villaverde AF, D Henriques, K Smallbone, S Bongard, J Schmid, D Cicin-Sain, A Crombach, J Saez-Rodriguez, K Mauch, E Balsa-Canto, P Mendes, J Jaeger and JR Banga (2015) BioPreDyn-bench: a suite of benchmark problems for dynamic modelling in systems biology. BMC Systems Biology 9:8.
