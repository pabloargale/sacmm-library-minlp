# REPRODUCIBILITY SCRIPTS

Example scripts using 11 MPI processors, and 1 openmp thread. Edit the parameters in the files as appropriate.

example_MM_B1.sh  ---> an embarrassingly-parallel non-cooperative multimethod, using benchmark B1
example_SACMM_B1.sh ---> a self-adaptive cooperative multimethod, using benchmarks B1
example_SACESS_B1.sh ---> a self-adaptive cooperative enhanced scatter search, using benchmarks B1
example_SACDE_B1.sh ---> a self-adaptive cooperative differential evolution, using benchmarks B1

example_MM_B2.sh  ---> an embarrassingly-parallel non-cooperative multimethod, using benchmark B2
example_SACMM_B2.sh ---> a self-adaptive cooperative multimethod, using benchmarks B2
example_SACESS_B2.sh ---> a self-adaptive cooperative enhanced scatter search, using benchmarks B2
example_SACDE_B2.sh ---> a self-adaptive cooperative differential evolution, using benchmarks B2

...

example_MM_B5.sh  ---> an embarrassingly-parallel non-cooperative multimethod, using benchmark B5
example_SACMM_B5.sh ---> a self-adaptive cooperative multimethod, using benchmarks B5
example_SACESS_B5.sh ---> a self-adaptive cooperative enhanced scatter search, using benchmarks B5
example_SACDE_B5.sh ---> a self-adaptive cooperative differential evolution, using benchmarks B5


